<?php


use Phinx\Seed\AbstractSeed;

class LanguageSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            ['label' => 'html', 'img' => 'img/language/html5.png'],
            ['label' => 'java', 'img' => 'img/language/java.png'],
            ['label' => 'php', 'img' => 'img/language/php.png'],
            ['label' => 'css', 'img' => 'img/language/css.png']
        ];
        $this->table('language')
            ->insert($data)
            ->save();
    }
}
