<?php

use Phinx\Migration\AbstractMigration;

class CreateSnippetTagTable extends AbstractMigration
{
    /**
     * Change Method.
     *
     * Write your reversible migrations using this method.
     *
     * More information on writing migrations is available here:
     * http://docs.phinx.org/en/latest/migrations.html#the-abstractmigration-class
     *
     * The following commands can be used in this method and Phinx will
     * automatically reverse them when rolling back:
     *
     *    createTable
     *    renameTable
     *    addColumn
     *    addCustomColumn
     *    renameColumn
     *    addIndex
     *    addForeignKey
     *
     * Any other destructive changes will result in an error when trying to
     * rollback the migration.
     *
     * Remember to call "create()" or "update()" and NOT "save()" when working
     * with the Table class.
     */
    public function change()
    {
        $table = $this->table('snippet_tag', ['id' => false, 'primary_key' => ['snippetId', 'tagId']]);

        $table->addColumn('snippetId', 'integer', ['null' => false])
            ->addForeignKey('snippetId', 'snippet', 'id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->addColumn('tagId', 'integer', ['null' => false])
            ->addForeignKey('tagId', 'tag', 'id', ['delete' => 'CASCADE', 'update' => 'CASCADE'])
            ->create();
    }
}
