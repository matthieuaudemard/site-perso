<?php


use Phinx\Seed\AbstractSeed;

class SnippetSeeder extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeders is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     */
    public function run()
    {
        $data = [
            'title' => htmlentities('A Basic HTML5 Template For Any Project'),
            'code' => htmlentities('<!doctype html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title>The HTML5 Herald</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="SitePoint">

  <link rel="stylesheet" href="css/styles.css?v=1.0">

</head>

<body>
  <script src="js/scripts.js"></script>
</body>
</html>'),
            'text' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque rhoncus euismod erat quis
                       feugiat. Cras tempor nisi sed neque pellentesque, nec congue enim feugiat. Integer sit amet
                       elementum justo. Quisque pellentesque neque sed sem consectetur, ac imperdiet magna aliquet.
                       Quisque suscipit rutrum convallis. Interdum et malesuada fames ac ante ipsum primis in faucibus.
                       Nunc dapibus velit arcu, eget vulputate justo porta nec. Duis ultrices sit amet quam sit amet
                       dictum. Quisque quis mattis est. Nam dictum malesuada tincidunt. Duis vulputate augue sed massa
                       suscipit hendrerit. Phasellus risus nisl, malesuada sed orci sit amet, posuere tempor diam.
                       Etiam iaculis iaculis turpis non efficitur. Aenean sagittis id augue eget aliquet. Donec
                       convallis nulla molestie sagittis porttitor. Vestibulum in scelerisque eros.',
            'languageId' => 1,
            'date' => (new DateTime())->format('Y-m-d H:i:s'),

        ];
        $this->table('snippet')
            ->insert($data)
            ->save();
    }
}
