<?php

use App\Controller\DefaultController;
use App\Controller\SnippetController;
use Core\App;
use DI\ContainerBuilder;
use GuzzleHttp\Psr7\ServerRequest;
use function Http\Response\send;

require_once dirname(__DIR__) . '/vendor/autoload.php';


$builder = new ContainerBuilder();
$builder->useAutowiring(true);
$builder->addDefinitions(dirname(__DIR__) . '/config.php');
$container = $builder->build();
$app = new App($container, [
    SnippetController::class,
    DefaultController::class
]);

$response = $app->run(ServerRequest::fromGlobals());

send($response);
