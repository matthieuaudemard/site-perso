<?php

namespace test\app;

use App\Controller\SnippetController;
use Core\App;
use DI\ContainerBuilder;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;

class AppTest extends TestCase
{
    /**
     * @var App
     */
    private $app;

    public function testRedirectTrailingSlash()
    {
        $resquest_uri = '/azerty';
        $request = new ServerRequest('GET', $resquest_uri . '/');
        $response = $this->app->run($request);
        self::assertContains($resquest_uri, $response->getHeader('Location'));
        self::assertEquals(301, $response->getStatusCode());
    }

    public function testSnippetIndex()
    {
        $request = new ServerRequest('GET', '/snippet');
        $response = $this->app->run($request);
        self::assertEquals(200, $response->getStatusCode());
    }

    public function testSnippetShow()
    {
        $requestSingle = new ServerRequest('GET', '/snippet/1');
        $responseSingle = $this->app->run($requestSingle);
        self::assertEquals(200, $responseSingle->getStatusCode());
    }

    public function testSnippetNotFound()
    {
        $requestSingle = new ServerRequest('GET', '/snippet/blablabla');
        $responseSingle = $this->app->run($requestSingle);
        self::assertEquals(404, $responseSingle->getStatusCode());
    }

    protected function setUp(): void
    {
        parent::setUp();
        $builder = new ContainerBuilder();
        $builder->useAutowiring(true);
        $builder->addDefinitions('config.php');
        $container = $builder->build();
        $this->app = new App($container, [SnippetController::class]);
    }
}
