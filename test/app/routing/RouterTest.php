<?php

namespace test\app\routing;

use Core\routing\Router;
use GuzzleHttp\Psr7\ServerRequest;
use PHPUnit\Framework\TestCase;

class RouterTest extends TestCase
{
    /**
     * @var Router
     */
    private $router;

    public function setUp(): void
    {
        parent::setUp();
        $this->router = new Router();
    }

    public function testGetMethod()
    {
        $request = new ServerRequest('GET', '/snippets');
        $this->router->get('/snippets', function () {
            return 'Hello';
        }, 'snippets');
        $route = $this->router->match($request);
        $this->assertEquals('snippets', $route->getName());
        $this->assertEquals('Hello', call_user_func_array($route->getCallable(), [$request]));
    }

    public function testGetMethodIfUrlDoesNotExists()
    {
        $request = new ServerRequest('GET', '/snippets');
        $this->router->get('/snippetzes', function () {
            return 'hello';
        }, 'snippet');
        $route = $this->router->match($request);
        self::assertNull($route);
    }

    public function testGetMethodWithParameters()
    {
        $request = new ServerRequest('GET', '/snippet/my-snipp-8');
        $this->router->get('/snippet', function () {
            return 'hello1';
        }, 'snippets');
        $this->router->get('/snippet/{slug:[a-z0-9\-]+}-{id:\d+}', function () {
            return 'helloSlug';
        }, 'snippet.show');
        $route = $this->router->match($request);
        self::assertEquals('snippet.show', $route->getName());
        self::assertEquals('helloSlug', call_user_func_array($route->getCallable(), [$request]));
        self::assertEquals(['slug' => 'my-snipp', 'id' => '8'], $route->getParams());
        // Test invalid url
        $route = $this->router->match(new ServerRequest('GET', '/snippet/my_snipp-8'));
        self::assertNull($route);
    }

    public function testGenerateUri()
    {
        $this->router->get('/snippet', function () {
            return 'blabla';
        }, 'snippets');
        $this->router->get('/snippet/{slug:[a-z0-9\-]+}-{id:\d+}', function () {
            return 'hello';
        }, 'snippet.show');
        $uri = $this->router->generateUri('snippet.show', ['slug' => 'my-snipp', 'id' => 8]);
        self::assertEquals('/snippet/my-snipp-8', $uri);
    }
}
