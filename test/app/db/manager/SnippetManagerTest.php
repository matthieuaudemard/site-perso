<?php


namespace Test\App\Db\Manager;

use App\Db\Entity\SnippetEntity;
use App\Db\Manager\SnippetManager;
use Core\Db\Entity\Entity;
use Core\Db\Manager\Manager;
use Core\Db\PdoDatabase;
use PDO;
use Phinx\Config\Config;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Console\Input\StringInput;
use Symfony\Component\Console\Output\NullOutput;

class SnippetManagerTest extends TestCase
{
    /**
     * @var Manager
     */
    protected $manager;

    /**
     * @var PDO
     */
    protected $pdo;

    protected function setUp(): void
    {
        parent::setUp();
        $this->pdo = new PDO('sqlite::memory:', null, null, []);
        $configArray = require 'phinx.php';
        $configArray['environments']['testing'] = [
            'adapter' => 'sqlite',
            'connection' => $this->pdo,
            'memory' => true
        ];
        $config = new Config($configArray);
        $manager = new \Phinx\Migration\Manager($config, new StringInput(''), new NullOutput());
        $manager->migrate('testing');
        $manager->seed('testing');
        $this->manager = new SnippetManager(new PdoDatabase($this->pdo));
    }

    public function testFindIdNotExists()
    {
        $tag = $this->manager->find(10000000000);
        self::assertNull($tag);
    }

    public function testFindAll()
    {
        $langs = $this->manager->findAll();
        self::assertIsArray($langs);
    }

    public function testFind()
    {
        $snippet = $this->manager->find(1);
        self::assertInstanceOf(Entity::class, $snippet);
        self::assertInstanceOf(SnippetEntity::class, $snippet);
    }
}
