<?php


namespace App\Model;

use Core\Model\Dto;
use DateTime;

class SnippetDto implements Dto
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $text;
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var string
     */
    private $requirement;
    /**
     * @var LanguageDto
     */
    private $language;
    /**
     * @var TagDto[]
     */
    private $tags = [];

    /**
     * @return int
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return DateTime
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     */
    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    /**
     * @return string
     */
    public function getRequirement(): ?string
    {
        return $this->requirement;
    }

    /**
     * @param string $requirement
     */
    public function setRequirement(?string $requirement): void
    {
        $this->requirement = $requirement;
    }

    /**
     * @return LanguageDto
     */
    public function getLanguage(): LanguageDto
    {
        return $this->language;
    }

    /**
     * @param LanguageDto $language
     */
    public function setLanguage(?LanguageDto $language): void
    {
        $this->language = $language;
    }

    /**
     * @return TagDto[]
     */
    public function getTags(): array
    {
        return $this->tags;
    }

    /**
     * @param TagDto[] $tags
     */
    public function setTags(array $tags): void
    {
        $this->tags = $tags;
    }
}
