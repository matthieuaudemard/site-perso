<?php


namespace App\Service;

use App\Model\SnippetDto;

interface SnippetService
{
    /**
     * Recherche tous les snippets
     * @return SnippetDto[]
     */
    public function findAll(): array;

    /**
     * Recherche un snippet par son id
     * @param int $id
     * @return SnippetDto
     */
    public function findById(int $id): ?SnippetDto;

    /**
     * Recherche tous les snippets de tagId $id
     * @param int $id
     * @return SnippetDto[]
     */
    public function findByTagId(int $id): array;

    /**
     * Recherche tous les snippets respectant les critères en paramètres
     * @param array $criteria tableau de type [clé => valeur] contenant les différents critères de recherche
     * @return SnippetDto[]
     */
    public function findAllWithCriteria(array $criteria = []): array;
}
