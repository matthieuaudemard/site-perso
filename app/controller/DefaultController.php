<?php


namespace App\Controller;

use Core\Controller\Controller;
use Core\Routing\Router;
use Psr\Http\Message\ServerRequestInterface as Request;

class DefaultController extends Controller
{
    /**
     * @var string
     */
    protected $template = 'snippet';

    /**
     * DefaultController constructor.
     * @param Router $router
     */
    public function __construct(Router $router)
    {
        parent::__construct($router);
        $this->viewPath = dirname(__DIR__) . '/view/';
        $this->router->get('/', [$this, 'index'], 'default.index');
    }

    public function index(Request $request): string
    {
        $this->render('default.index');
        return '';
    }
}
