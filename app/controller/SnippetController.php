<?php


namespace App\Controller;

use App\Service\SnippetService;
use Core\Controller\Controller;
use Core\Routing\Router;
use Psr\Http\Message\ServerRequestInterface as Request;

class SnippetController extends Controller
{
    /**
     * @var string
     */
    protected $template = 'snippet';


    /**
     * @var SnippetService
     */
    protected $snippetService;

    /**
     * SnippetModule constructor.
     * @param Router $router
     * @param SnippetService $snippetService
     */
    public function __construct(Router $router, SnippetService $snippetService)
    {
        parent::__construct($router);
        $this->router->get('/snippet', [$this, 'index'], 'snippet.index');
        $this->router->get('/snippet/{id:[1-9]+}', [$this, 'show'], 'snippet.show');
        $this->router->get('/snippet/tag/{id:[1-9]+}', [$this, 'byTag'], 'snippet.bytag');
        $this->router->get('/snippet/search', [$this, 'search'], 'snippet.search');
        $this->viewPath = dirname(__DIR__) . '/view/';
        $this->snippetService = $snippetService;
    }

    public function index(Request $request): string
    {
        $snippets = $this->snippetService->findAll();
        $this->render('snippet.index', ['snippets' => $snippets]);
        return '';
    }

    public function show(Request $request): string
    {
        $id = (int)$request->getAttribute('id');
        $snippet = $this->snippetService->findById($id);
        $this->render('snippet.show', ['snippet' => $snippet]);
        return '';
    }

    public function byTag(Request $request): string
    {
        $snippets = $this->snippetService->findByTagId($request->getAttribute('id'));
        $this->render('snippet.index', ['snippets' => $snippets]);
        return '';
    }

    public function search(Request $request): string
    {
        $queryString = htmlspecialchars_decode($request->getQueryParams()['q']);
        $keyword = $queryString;
        $criterias = [];

        // on vérifie l'existence de séquences du type tag:value dans la chaîne de recherche
        if (preg_match('#tag:[0-9A-Za-z.-_]+#', $keyword, $tags)) {
            $keyword = preg_replace('#tag:[0-9A-Za-z.-_]+#', '', $keyword);
            $criterias['tag'] = str_replace('tag:', '', $tags[0]);
        }

        // on vérifie l'existence de séquences du type lang:value dans la chaîne de recherche
        if (preg_match('#lang:[0-9A-Za-z.-_]+#', $keyword, $languages)) {
            $keyword = preg_replace('#lang:[0-9A-Za-z.-_]+#', '', $keyword);
            $criterias['lang'] = str_replace('lang:', '', $languages[0]);
        }

        $keyword = trim($keyword);

        if (strlen($keyword) > 0) {
            $criterias['keyword'] = '%' . trim($keyword) . '%';
        }
        $snippets = $this->snippetService->findAllWithCriteria($criterias);
        $this->render('snippet.index', compact('snippets', 'queryString'));
        return '';
    }
}
