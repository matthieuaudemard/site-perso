<?php


namespace App\Service;

use App\Db\Manager\LanguageManager;
use App\Model\LanguageDto;
use Core\Converter\Converter;
use Exception;
use ReflectionException;

class LanguageServiceImpl implements LanguageService
{
    /**
     * @var LanguageManager
     */
    private $languageManager;

    /**
     * @var Converter
     */
    private $converter;

    /**
     * LanguageService constructor.
     * @param $languageManager
     * @throws Exception
     */
    public function __construct(LanguageManager $languageManager)
    {
        $this->languageManager = $languageManager;
        $this->converter = new Converter(LanguageDto::class);
    }

    /**
     * @param int $id
     * @return LanguageDto
     * @throws ReflectionException
     */
    public function findById(int $id): LanguageDto
    {
        return $this->converter->convert($this->languageManager->find($id));
    }
}
