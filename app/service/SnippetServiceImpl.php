<?php


namespace App\Service;

use App\Db\Entity\SnippetEntity;
use App\Db\Manager\SnippetManager;
use App\Model\SnippetDto;
use Core\Converter\Converter;
use Exception;
use ReflectionException;

class SnippetServiceImpl implements SnippetService
{

    /**
     * @var SnippetManager
     */
    private $snippetManager;

    /**
     * @var LanguageService
     */
    private $languageService;

    /**
     * @var TagService
     */
    private $tagService;

    /**
     * @var Converter
     */
    private $converter;


    /**
     * SnippetService constructor.
     * @param SnippetManager $snippetManager
     * @param LanguageService $languageService
     * @param TagService $tagService
     * @throws Exception
     */
    public function __construct(
        SnippetManager $snippetManager,
        LanguageService $languageService,
        TagService $tagService
    ) {
        $this->snippetManager = $snippetManager;
        $this->languageService = $languageService;
        $this->tagService = $tagService;
        $this->converter = new Converter(SnippetDto::class);
    }

    /**
     * @return SnippetDto[]
     */
    public function findAll(): array
    {
        $all = $this->snippetManager->findAll();
        return array_map([$this, 'convertToDtoAndLinkForeignKeys'], $all);
    }

    /**
     * @param int $id
     * @return SnippetDto
     * @throws ReflectionException
     */
    public function findById(int $id): ?SnippetDto
    {
        $entity = $this->snippetManager->find($id);
        if (!is_null($entity)) {
            return $this->convertToDtoAndLinkForeignKeys($entity);
        }
        return null;
    }

    /**
     * @param SnippetEntity $snippet
     * @return SnippetDto
     * @throws ReflectionException
     */
    private function convertToDtoAndLinkForeignKeys(SnippetEntity $snippet): SnippetDto
    {
        $dto = $this->converter->convert($snippet);
        $dto->setLanguage($this->languageService->findById($snippet->getLanguageId()));
        $dto->setTags($this->tagService->findBySnippetId($snippet->getId()));
        return $dto;
    }

    /**
     * Recherche tous les snippets de tagId $id
     * @param int $id
     * @return SnippetDto[]
     */
    public function findByTagId(int $id): array
    {
        $entities = $this->snippetManager->findByTagId($id);
        return array_map([$this, 'convertToDtoAndLinkForeignKeys'], $entities);
    }

    /**
     * Recherche tous les snippets respectant les critères en paramètres
     * @param array $criteria tableau de type [clé => valeur] contenant les différents critères de recherche
     * @return SnippetDto[]
     */
    public function findAllWithCriteria(array $criteria = []): array
    {
        $entities = $this->snippetManager->findAllWithCriteria($criteria);
        return array_map([$this, 'convertToDtoAndLinkForeignKeys'], $entities);
    }
}
