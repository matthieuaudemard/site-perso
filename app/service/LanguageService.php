<?php


namespace App\Service;

use App\Model\LanguageDto;

interface LanguageService
{
    /**
     * @param int $id
     * @return mixed
     */
    public function findById(int $id): LanguageDto;
}
