<?php


namespace App\Service;

use App\Model\TagDto;

interface TagService
{
    /**
     * @param int $id
     * @return TagDto[]
     */
    public function findBySnippetId(int $id): array;
}
