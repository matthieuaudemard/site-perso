<?php


namespace App\Service;

use App\Db\Manager\TagManager;
use App\Model\TagDto;
use Core\Converter\Converter;
use Exception;
use ReflectionException;

class TagServiceImpl implements TagService
{
    /**
     * @var TagManager
     */
    private $tagManager;

    /**
     * @var Converter
     */
    private $converter;

    /**
     * LanguageService constructor.
     * @param TagManager $tagManager
     * @throws Exception
     */
    public function __construct(TagManager $tagManager)
    {
        $this->tagManager = $tagManager;
        $this->converter = new Converter(TagDto::class);
    }

    /**
     * @param int $id
     * @return TagDto[]
     * @throws ReflectionException
     */
    public function findBySnippetId(int $id): array
    {
        $entities = $this->tagManager->findBySnippetId($id);
        if (is_array($entities)) {
            return $this->converter->convertAll($entities);
        }
        return [];
    }
}
