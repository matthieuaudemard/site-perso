<?php


namespace App\Db\Manager;

use Core\Db\Manager\Manager;

class TagManager extends Manager
{

    public function findBySnippetId(int $id)
    {
        return $this->query(
            'SELECT * FROM tag JOIN snippet_tag st on tag.id = st.tagId WHERE st.snippetId = :id',
            ['id' => $id]
        );
    }
}
