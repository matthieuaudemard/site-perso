<?php


namespace App\Db\Manager;

use Core\Db\Manager\Manager;

class SnippetManager extends Manager
{

    public function findByTagId(int $id)
    {
        $sql = 'SELECT * FROM snippet JOIN snippet_tag st on snippet.id = st.snippetId WHERE snippetId = :id';
        $results = $this->query($sql, ['id' => $id]);
        return !is_null($results) && $results ? $results : [];
    }

    /**
     * @param string[] $criteria
     * @return array
     */
    public function findAllWithCriteria(array $criteria = []): array
    {
        $query = 'SELECT s.* FROM SNIPPET s';
        $clauses = [];

        if (array_key_exists('lang', $criteria)) {
            $query .= ' JOIN LANGUAGE l ON s.languageId = l.id';
            $clauses[] = 'l.label = :lang';
        }

        if (array_key_exists('tag', $criteria)) {
            $query .= ' JOIN SNIPPET_TAG st ON st.snippetId = s.id';
            $query .= ' JOIN TAG t ON st.tagId = t.id';
            $clauses[] = 't.label = :tag';
        }

        if (array_key_exists('keyword', $criteria)) {
            $clauses[] = '(s.title LIKE :keyword OR ' .
                's.code LIKE :keyword OR ' .
                's.text LIKE :keyword OR ' .
                's.requirement = :keyword)';
        }

        if (sizeof($clauses) > 0) {
            $query .= ' WHERE ' . join(' AND ', $clauses);
        }

        $results = $this->query($query, $criteria);
        return !is_null($results) && $results ? $results : [];
    }
}
