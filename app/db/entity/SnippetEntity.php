<?php


namespace App\Db\Entity;

use Core\Db\Entity\Entity;
use Core\Formatter\DateFormatter;
use DateTime;

class SnippetEntity implements Entity
{
    /**
     * @var int
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var string
     */
    private $code;
    /**
     * @var string
     */
    private $text;
    /**
     * @var DateTime
     */
    private $date;
    /**
     * @var string
     */
    private $requirement;
    /**
     * @var int
     */
    private $languageId;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(?int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(?string $title): void
    {
        $this->title = $title;
    }

    /**
     * @return string|null
     */
    public function getCode(): ?string
    {
        return $this->code;
    }

    /**
     * @param string $code
     */
    public function setCode(?string $code): void
    {
        $this->code = $code;
    }

    /**
     * @return string|null
     */
    public function getText(): ?string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(?string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return is_string($this->date) ? DateFormatter::format($this->date) : $this->date;
    }

    /**
     * @param string|DateTime $date
     */
    public function setDate($date): void
    {
        if (is_string($date)) {
            $date = DateFormatter::format($this->date);
        }
        $this->date = $date;
    }

    /**
     * @return string|null
     */
    public function getRequirement(): ?string
    {
        return $this->requirement;
    }

    /**
     * @param string $requirement
     */
    public function setRequirement(?string $requirement): void
    {
        $this->requirement = $requirement;
    }

    /**
     * @return int|null
     */
    public function getLanguageId(): ?int
    {
        return $this->languageId;
    }

    /**
     * @param int $languageId
     */
    public function setLanguageId(?int $languageId): void
    {
        $this->languageId = $languageId;
    }
}
