<?php

?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Page Title</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/style/style.css">
    <link rel="stylesheet" href="/style/lib.css">
    <link rel="stylesheet" crossorigin="anonymous"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
          href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet"
          href="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.18.1/styles/github.min.css">
</head>
<body>

<!-- The flexible grid (content) -->
<div class="row">
    <div class="side text-center">
        <h2>Matthieu Audemard</h2>
        <img class="round" alt="avatar"
             src="https://assets.gitlab-static.net/uploads/-/system/user/avatar/3155144/avatar.png?width=23">
        <p class="font-medium">Bienvenue sur mon site personnel</p>
        <div class="social">
            <a href="https://github.com/matthieuaudemard"><i class="fa fa-github-alt"></i></a>
            <a href="https://www.linkedin.com/in/matthieu-audemard-a30518154/"><i class="fa fa-linkedin"></i></a>
        </div>
        <hr>
        <nav class="text-left">
            <ul class="fa-ul">
                <li class="nav-item">
                    <a class="nav-link" href=""><i class="fa fa-user fa-fw mr-2"></i>About Me</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/snippet/"><i class="fa fa-code fa-fw mr-2"></i>Snippets</a>
                </li>
            </ul>
        </nav>
    </div>
    <div class="main">
        <?= $content ?>
    </div>

</div>

<!-- Footer -->
<div class="footer">
    <h2>Footer</h2>
</div>
<script src="//cdnjs.cloudflare.com/ajax/libs/highlight.js/9.18.1/highlight.min.js"></script>
<script>hljs.initHighlightingOnLoad();</script>
</body>
</html>