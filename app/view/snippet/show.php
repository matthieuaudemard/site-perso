<section class="wrapper">
    <?php

    use App\Model\SnippetDto;

    if (isset($snippet) && $snippet instanceof SnippetDto) : ?>
        <div class="container">
            <div class="item">
                <div class="media">
                    <div class="media-body">
                        <h2><?= $snippet->getTitle() ?></h2>
                        <?php
                        $tagDtos = $snippet->getTags();
                        if (!empty($tagDtos)) :
                            $tags = [];
                            // Construction de la liste de liens de tags
                            foreach ($tagDtos as $tag) {
                                $link = $this->router->generateUri('snippet.bytag', ['id' => $snippet->getId()]);
                                $tags[] = '<a href="' . $link . '">' . $tag->getLabel() . '</a>';
                            }
                            ?>
                            <p class="tag-list"><?= join(', ', $tags) ?></p>
                            <?php
                        endif; ?>
                        <pre class="<?= $snippet->getLanguage()->getLabel() ?>">
                        <code><?= $snippet->getCode() ?></code>
                    </pre>
                        <?php
                        if (!is_null($snippet->getText())) {
                            if (strlen($snippet->getText()) > 300) {
                                echo '<p class="snip-text">' . $snippet->getText() . ' ...</p>';
                            } else {
                                echo '<p class="snip-text">' . $snippet->getText() . '</p>';
                            }
                        }
                        ?>
                    </div><!--//media-body-->
                </div><!--//media-->
            </div><!--//item-->
        </div>
    <?php else : ?>
        <p>Le snippet n'existe pas !!</p>
    <?php endif; ?>
</section>
