<section class="header">
    <div class="container text-center">
        <h1 class="heading"><a href="/snippet">Sniphp - Gestionnaire de snippets en php</a></h1>
        <form class="search-form" method="get" action="/snippet/search">
            <div class="search-form-wrapper">
                <label class="sr-only" for="search">Recherche</label>
                <input type="text" id="search" name="q" class="" placeholder="i.e : tag:web lang:html template"
                       value="<?= isset($queryString) ? $queryString : '' ?>">
                <button type="submit" class="rounded">recherche</button>
            </div>
        </form>
    </div><!--//container-->
</section>

<section class="wrapper">
    <div class="container">
        <?php

        use App\Model\SnippetDto;

        if (isset($snippets) && is_array($snippets)) :
            foreach ($snippets as $snippet) :
                if ($snippet instanceof SnippetDto) :
                    $router = $this->router;
                    $link = $router->generateUri('snippet.show', ['id' => $snippet->getId()]); ?>
                    <div class="item">
                        <div class="media">
                            <img src="<?= '/' . $snippet->getLanguage()->getImg() ?>"
                                 alt="<?= $snippet->getLanguage()->getLabel() ?>"
                                 width="100" height="100">
                            <div class="media-body">
                                <h2><a href="<?= $link ?>"><?= $snippet->getTitle() ?></a></h2>
                                <?php
                                $tagDtos = $snippet->getTags();
                                if (!empty($tagDtos)) :
                                    $tags = [];
                                    // Construction de la liste de liens de tags
                                    foreach ($tagDtos as $tag) {
                                        $tags[] = '<a href="' .
                                            $router->generateUri('snippet.bytag', ['id' => $snippet->getId()]) .
                                            '">' . $tag->getLabel() . '</a>';
                                    }
                                    ?>
                                    <p class="tag-list"><?= join(', ', $tags) ?></p>
                                    <?php
                                endif;
                                if (!is_null($snippet->getText())) {
                                    if (strlen($snippet->getText()) > 300) {
                                        echo '<p class="snip-text">' . substr($snippet->getText(), 0, 300) . ' ...</p>';
                                    } else {
                                        echo '<p class="snip-text">' . $snippet->getText() . '</p>';
                                    }
                                }
                                ?>
                                <a class="more-link"
                                   href="<?= $link ?>">
                                    Read more
                                </a>
                            </div><!--//media-body-->
                        </div><!--//media-->
                    </div><!--//item-->
                <?php
                endif;
            endforeach;
        endif; ?>
    </div>
</section>