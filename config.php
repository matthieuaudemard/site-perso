<?php

use App\Controller\DefaultController;
use App\Controller\SnippetController;
use App\Db\Manager\LanguageManager;
use App\Db\Manager\SnippetManager;
use App\Db\Manager\TagManager;
use App\Service\LanguageService;
use App\Service\LanguageServiceImpl;
use App\Service\SnippetService;
use App\service\SnippetServiceImpl;
use App\Service\TagService;
use App\Service\TagServiceImpl;
use Core\App;
use Core\Db\Database;
use Core\Db\PdoDatabase;
use Core\routing\Router;
use function DI\autowire;
use function DI\create;
use function DI\get;

return [
    'db.name' => \DI\env('db_user', 'sniphp'),
    'db.host' => \DI\env('db_user', 'localhost'),
    'db.user' => \DI\env('db_user', 'root'),
    'db.pass' => \DI\env('db_user', ''),
    PDO::class => autowire()->constructor(
        'mysql:dbname=sniphp;host=localhost',
        get('db.user'),
        get('db.pass'),
        []
    ),
    PdoDatabase::class => autowire()->constructor(get(PDO::class)),
    Database::class => get(PdoDatabase::class),


    App::class => create()->constructor(get(Database::class)),

    // Managers
    TagManager::class => autowire()->constructor(get(Database::class)),
    LanguageManager::class => autowire()->constructor(get(Database::class)),
    SnippetManager::class => autowire()->constructor(get(Database::class)),

    // Services Implementations
    TagServiceImpl::class => autowire()->constructor(get(TagManager::class)),
    LanguageServiceImpl::class => autowire()->constructor(get(LanguageManager::class)),
    SnippetServiceImpl::class => autowire()->constructor(
        get(SnippetManager::class),
        get(LanguageServiceImpl::class),
        get(TagServiceImpl::class)
    ),

    // Services Interfaces
    TagService::class => get(TagServiceImpl::class),
    LanguageService::class => get(LanguageServiceImpl::class),
    SnippetService::class => get(SnippetServiceImpl::class),

    // Controllers
    SnippetController::class => autowire()->constructor(
        get(Router::class),
        get(SnippetServiceImpl::class)
    ),
    DefaultController::class => autowire()->constructor(
        get(Router::class)
    ),

    Router::class => autowire(),
];
