<?php

// Ne pas inclure ce fichier lors des tests
if (php_sapi_name() !== 'cli') {
    require_once 'public/index.php';
    var_dump($app);
}

$fromApp = isset($app);

return [
    'paths' => [
        'migrations' => __DIR__ . '/db',
        'seeds' => __DIR__ . '/db'
    ],
    'environments' => [
        'default_migration_table' => 'phinxlog',
        'default_database' => 'development',
        'development' => [
            'adapter' => 'mysql',
            'host' => !$fromApp ? 'localhost' : $app->getContainer()->get('db.host'),
            'name' => !$fromApp ? 'sniphp' : $app->getContainer()->get('db.name'),
            'user' => !$fromApp ? 'root' : $app->getContainer()->get('db.user'),
            'pass' => !$fromApp ? '' : $app->getContainer()->get('db.pass'),
            'port' => '3306',
            'charset' => 'utf8'
        ],
    ]
];
