<?php


namespace Core\Controller;

use Core\routing\Router;

abstract class Controller
{
    /**
     * @var string nom du template à utiliser
     */
    protected $template;

    /**
     * @var string chemin d'accès au template
     */
    protected $viewPath;

    /**
     * @var Router
     */
    protected $router;

    /**
     * Controller constructor.
     */
    public function __construct(Router $router)
    {
        $this->router = $router;
    }


    /**
     * Permet de mettre en forme les données contenues dans $variables en les injectant dans la vue $view.
     * @param $view string
     *      nom de la vue à afficher
     * @param array $variables
     *      tableau [ 'var_name' => value ] à injecter dans la vue.
     *      toutes les clés 'var_name' seront extraites sous forme de variables $var_name.
     */
    protected function render(string $view, array $variables = [])
    {
        ob_start();
        extract($variables);
        require $this->viewPath . str_replace('.', '/', $view) . '.php';
        $content = ob_get_clean();
        require $this->viewPath . 'template/' . $this->template . '.php';
    }
}
