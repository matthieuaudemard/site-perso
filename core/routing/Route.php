<?php


namespace Core\Routing;

use Psr\Http\Server\MiddlewareInterface;

/**
 * Class Route
 * @package Core\routing
 */
class Route
{
    /**
     * @var string
     */
    private $name;
    /**
     * @var MiddlewareInterface
     */
    private $middleware;
    /**
     * @var array
     */
    private $parameters;

    /**
     * Route constructor.
     * @param string $name
     * @param callable $callable
     * @param array $parameters
     */
    public function __construct(string $name, callable $callable, array $parameters)
    {
        $this->name = $name;
        $this->middleware = $callable;
        $this->parameters = $parameters;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return callable
     */
    public function getCallable(): callable
    {
        return $this->middleware;
    }

    /**
     * Permet de récupérer les paramètres de l'URI
     * @return string[] liste des paramètres
     */
    public function getParams(): array
    {
        return $this->parameters;
    }
}
