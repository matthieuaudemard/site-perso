<?php


namespace Core\Routing;

use Mezzio\Router\FastRouteRouter;
use Mezzio\Router\Route as MerrioRoute;
use Psr\Http\Message\ServerRequestInterface;

/**
 * Class Router
 * @package Core\routing
 */
class Router
{
    /**
     * @var FastRouteRouter
     */
    private $router;

    public function __construct()
    {
        $this->router = new FastRouteRouter();
    }


    /**
     * Permet d'ajouter une nouvelle route utilisant la méthode GET.
     * @param string $path
     * @param callable $callable
     * @param string|null $name
     */
    public function get(string $path, callable $callable, ?string $name = null)
    {
        $this->router->addRoute(new MerrioRoute($path, new MiddlewareApp($callable), ['GET'], $name));
    }

    /**
     * Renvoie une route si la requête $request correspond à une route paramétrée dans le routeur. Sinon renvoie null.
     * @param ServerRequestInterface $request
     * @return Route|null
     */
    public function match(ServerRequestInterface $request): ?Route
    {
        $result = $this->router->match($request);
        if ($result->isSuccess()) {
            return new Route(
                $result->getMatchedRouteName(),
                $result->getMatchedRoute()->getMiddleware()->getCallback(),
                $result->getMatchedParams()
            );
        }
        return null;
    }

    /**
     * Permet de générer l'uri correspondant à la route $name à l'aide des paramètres $params.
     * @param string $name nom de la route
     * @param array $params tableaux de paramètres
     * @return string|null
     */
    public function generateUri(string $name, array $params): ?string
    {
        return $this->router->generateUri($name, $params);
    }
}
