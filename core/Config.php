<?php

namespace Core;

class Config
{
    /**
     * @var Config
     */
    private static $instance;

    /**
     * @var array|mixed
     */
    private $settings = [];

    public function __construct($file)
    {
        $this->settings = require $file;
    }

    /**
     * Permet de charger une configuration
     * @param string $file
     * @return Config
     */
    public static function getInstance(string $file): Config
    {
        if (self::$instance === null) {
            self::$instance = new Config($file);
        }
        return self::$instance;
    }

    /**
     * Permet d'obtenir la valeur associée à la clé passé en paramètres
     * @param string $key
     * @return string|null
     */
    public function get(string $key): ?string
    {
        if (!isset($this->settings[$key])) {
            return null;
        }
        return $this->settings[$key];
    }
}
