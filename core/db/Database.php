<?php


namespace Core\Db;

use PDO;

interface Database
{
    public function query(string $statement, string $str_replace, bool $one);

    public function prepare(string $statement, array $attributes, string $str_replace, bool $one);

    public function exec(string $statement);

    public function lastInsertId();

    public function getPdo(): PDO;
}
