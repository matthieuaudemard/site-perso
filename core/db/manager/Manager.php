<?php


namespace Core\Db\Manager;

use Core\Db\Database;
use Core\Db\Entity\Entity;

abstract class Manager
{
    /**
     * @var Database
     */
    protected $db;

    /**
     * @var string
     */
    protected $table;

    /**
     * Manager constructor.
     * @param Database $db
     */
    public function __construct(Database $db)
    {
        $this->db = $db;
        if ($this->table === null) {
            $chunks = explode('\\', get_class($this));
            $class_name = end($chunks);
            $this->table = strtolower(str_replace('Manager', '', $class_name));
        }
    }

    public function extract($key, $value)
    {
        $return = [];
        $records = $this->findAll();
        foreach ($records as $v) {
            $return[$v->$key] = $v->$value;
        }
        return $return;
    }

    /**
     * Effectue une requête SELECT * sur la table $this->table
     * @return Entity[] Retourne le résultat de la requête sous forme de tableau d'objets.
     */
    public function findAll(): array
    {
        return $this->query('SELECT * FROM ' . $this->table) ?: [];
    }

    /**
     * Execute la requête $statement.
     * @param string $statement la requête à exécuter
     * @param array|null $attributes dans le cas d'une requête préparer tableau de [clé] => [valeur]
     * @param bool $one permet de spécifier si l'on attend un seul ou plusieurs résultats
     * @return mixed
     */
    public function query(string $statement, array $attributes = null, bool $one = false)
    {
        if ($attributes) {
            return $this->db->prepare(
                $statement,
                $attributes,
                str_replace('Manager', 'Entity', get_class($this)),
                $one
            )?: null;
        } else {
            return $this->db->query(
                $statement,
                str_replace('Manager', 'Entity', get_class($this)),
                $one
            )?: null;
        }
    }

    /**
     * Renvoie les derniers éléments créé sur la table.
     * @param int $count spécifie le nombre d'éléments maximum à renvoyer
     * @return Entity Renvoie les $count derniers éléments créés sur la table $this->table
     */
    public function getLast(int $count = null): ?Entity
    {
        $query = 'SELECT * FROM ' . $this->table . ' ORDER BY id DESC';
        if ($count and is_numeric($count)) {
            $query .= ' LIMIT ' . $count;
        }
        return $this->query($query, null, $count && $count == 1) ?: null;
    }

    public function find($id)
    {
        return $this->query(
            "SELECT * FROM " . $this->table . " WHERE " . $this->table . ".id = ?",
            [$id],
            true
        );
    }

    /**
     * @param $id int id de l'éléments à mettre à jour
     * @param $fields array de paramêtre à mettre à jour ['param1' => 'new value', ... ]
     * @return mixed
     */
    public function update($id, $fields): ?Entity
    {
        $sql_parts = [];
        $attributes = [];
        foreach ($fields as $k => $v) {
            $sql_parts [] = $k . ' = ?';
            $attributes [] = $v;
        }
        $attributes [] = $id;
        $sql_part = implode(', ', $sql_parts);
        return $this->query(
            'UPDATE ' . $this->table . ' SET ' . $sql_part . ' WHERE id = ?',
            $attributes,
            true
        );
    }

    /**
     * Lance une requête de suppression sur l'élement d'id $id.
     * @param int $id l'id de l'elément
     * @return mixed
     */
    public function delete(int $id)
    {
        return $this->query(
            'DELETE FROM ' . $this->table . ' WHERE id = ?',
            [$id],
            true
        );
    }

    /**
     * Lance une requête de création à partir du tableau [clé]=>[valeur] $fields correspondant aux colonnes
     * de la table à valuer.
     * @param $fields array tableau de paramètres à enter pour créer l'entité ['param1' => 'value1', ... ]
     * @return mixed
     */
    public function create(array $fields): ?Entity
    {
        $sql_parts = [];
        $attributes = [];
        foreach ($fields as $k => $v) {
            $sql_parts [] = $k . ' = ?';
            $attributes [] = $v;
        }
        $sql_part = implode(', ', $sql_parts);
        return $this->query(
            'INSERT INTO ' . $this->table . ' SET ' . $sql_part,
            $attributes,
            true
        );
    }
}
