<?php


namespace Core\Db;

use PDO;
use PDOException;
use PDOStatement;

class PdoDatabase implements Database
{
    /**
     * @var PDO
     */
    private $pdo;

    /**
     * MysqlDatabase constructor.
     * @param PDO $pdo
     */
    public function __construct(PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    /**
     * Permet de savoir si une requête doit être fetchée.
     * @param string $statement La requête SQL.
     * @return bool Renvoie true si la requête est fetchable, sinon false
     */
    private function isFetchable(string $statement)
    {
        return strpos($statement, 'UPDATE') !== 0 or
            strpos($statement, 'INSERT') !== 0 or
            strpos($statement, 'DELETE') !== 0;
    }

    /**
     * * Fetches la ligne suivante de $req
     * @param string $class_name nom de la classe d'objet à fetcher
     * @param bool $one true si un résultat unique est attendu, false sinon
     * @param PDOStatement $req la requête à fetcher
     * @return array|mixed
     *      si $class_name est null, alors renvoie un résultat sous forme de tableau associatif, sinon sous forme
     *      d'objet.
     */
    private function fetch(string $class_name, bool $one, PDOStatement $req)
    {
        if ($class_name === null) {
            $req->setFetchMode(PDO::FETCH_OBJ);
        } else {
            $req->setFetchMode(PDO::FETCH_CLASS, $class_name);
        }
        if ($one) {
            $data = $req->fetch();
        } else {
            $data = $req->fetchAll();
        }
        return $data;
    }

    /**
     * Exécute une requête SQL, retourne un résultat sous forme d'objet dont le nom de la classe est passé en paramètres
     * ou une tableau associatif si $class_name est null
     * @param string $statement requête SQL
     * @param string|null $class_name nom de la classe de l'objet
     * @param bool $one true si l'on attend qu'un seul résultat, false sinon
     * @return array|bool|mixed
     */
    public function query(string $statement, string $class_name = null, bool $one = false)
    {
        $req = $this->pdo->query($statement);
        // Si la requête est un UPDATE, DELETE ou INSERT -> pas de fetch.
        if (!$this->isFetchable($statement)) {
            return $req;
        }
        return $this->fetch($class_name, $one, $req);
    }


    /**
     * Exécute une requête SQL préparée, retourne un résultat sous forme d'objet dont le nom de la classe est passé en
     * paramètres ou une tableau associatif si $class_name est null
     * @param string $statement requête SQL
     * @param array $attributes
     * @param string|null $class_name nom de la classe de l'objet
     * @param bool $one true si l'on attend qu'un seul résultat, false sinon
     * @return array|bool|mixed
     */
    public function prepare(string $statement, array $attributes, string $class_name = null, bool $one = false)
    {
        $req = $this->pdo->prepare($statement);
        try {
            $res = $req->execute($attributes);
        } catch (PDOException $e) {
            throw $e;
        }
        // Si la requête est un UPDATE, DELETE ou INSERT -> pas de fetch !
        if (!$this->isFetchable($statement)) {
            return $res;
        }
        return $this->fetch($class_name, $one, $req);
    }

    /**
     * Lance la méthode PDO::exec
     * @see PDO::exec()
     * @param string $statement le code de la requête sql à exécuter
     * @return false|int si succès retourne le nombre de lignes affectées, faux sinon.
     */
    public function exec(string $statement)
    {
        return $this->pdo->exec($statement);
    }

    /**
     * Retourne l'identifiant de la dernière ligne insérée en bd.
     * @return string retourne une string représentant l'id de la dernière ligne insérée dans la base.
     */
    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }

    /**
     * @return PDO
     */
    public function getPdo(): PDO
    {
        return $this->pdo;
    }
}
