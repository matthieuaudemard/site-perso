<?php

namespace Core;

use Core\routing\Router;
use Exception;
use GuzzleHttp\Psr7\Response;
use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;

define('ROOT_DIR', substr(0, strlen(__DIR__) - strlen('/app/')));

class App
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * liste des modules
     * @var array
     */
    private $modules = [];

    /**
     * @var Router
     */
    private $router;

    /**
     * App constructor.
     * @param ContainerInterface $container le conteneur de dépendances
     * @param string[] $controller la liste des modules à charger
     */
    public function __construct(ContainerInterface $container, array $controller = [])
    {
        $this->container = $container;
        $this->router = $container->get(Router::class);
        foreach ($controller as $module) {
            $this->modules[] = $container->get($module);
        }
    }

    /**
     * Lance l'application :
     *  - démarre la session
     *  - route la requête
     * @param ServerRequestInterface $request
     * @return ResponseInterface
     * @throws Exception
     */
    public function run(ServerRequestInterface $request): ResponseInterface
    {
        $uri = $request->getUri()->getPath();

        // Si l’uri se termine par un '/', on redirige vers l’uri sans le dernier '/'
        // sauf s’il s’agit de la racine du site
        if (!empty($uri) && $uri[-1] === '/' && $request->getServerParams()['REQUEST_URI'] != '/') {
            return (new Response())
                ->withStatus(301)
                ->withHeader('Location', substr($uri, 0, -1));
        }
        $route = $this->router->match($request);

        // Redirection 404
        if (is_null($route)) {
            return new Response(404, [], '<h1>Erreur 404</h1>');
        }

        // Extraction des paramètres de la route
        foreach ($route->getParams() as $key => $val) {
            $request = $request->withAttribute($key, $val);
        }
        // Exécution de la méthode associée à la route
        $response = call_user_func_array($route->getCallable(), [$request]);

        // Renvoi de la réponse
        if (is_string($response)) {
            return new Response(200, [], $response);
        } elseif ($response instanceof ResponseInterface) {
            return $response;
        } else {
            throw new Exception('Response is not a string or instance of ResponseInterface');
        }
    }

    /**
     * @return ContainerInterface
     */
    public function getContainer(): ContainerInterface
    {
        return $this->container;
    }
}
