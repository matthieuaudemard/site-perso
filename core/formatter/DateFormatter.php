<?php


namespace Core\Formatter;

use DateTime;

class DateFormatter
{
    const FORMAT = 'Y-m-d H:i:s';

    public static function format(string $date)
    {
        return DateTime::createFromFormat(self::FORMAT, $date);
    }
}
