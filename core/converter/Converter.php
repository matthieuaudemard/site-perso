<?php


namespace Core\Converter;

use Core\Db\Entity\Entity;
use Core\Model\Dto;
use Exception;
use ReflectionClass;
use ReflectionException;

/**
 * Class Converter
 * Permet de convertir une Entity en son DTO
 * @package Core\Converter
 */
class Converter
{
    /**
     *
     * @var mixed classe
     */
    private $class;

    /**
     * Converter constructor.
     * @param $class
     * @throws Exception
     */
    public function __construct($class)
    {
        if (class_exists($class)) {
            $this->class = $class;
        } else {
            throw new Exception('Cannot find class ' . $class);
        }
    }

    /**
     * @param Entity[] $entities
     * @return Dto[]
     * @throws ReflectionException
     */
    public function convertAll(array $entities): array
    {
        $result = [];
        foreach ($entities as $entity) {
            $result[] = $this->convert($entity);
        }
        return $result;
    }

    /**
     * @param Entity $entity
     * @return Dto
     * @throws ReflectionException
     */
    public function convert(Entity $entity): Dto
    {
        $reflect = new ReflectionClass($entity);
        $props = $reflect->getProperties();
        $result = new $this->class();
        foreach ($props as $prop) {
            $set_method = 'set' . ucfirst($prop->getName());
            $get_method = 'get' . ucfirst($prop->getName());
            if (method_exists($result, $set_method) && method_exists($entity, $get_method)) {
                $result->$set_method($entity->$get_method());
            }
        }
        return $result;
    }
}
